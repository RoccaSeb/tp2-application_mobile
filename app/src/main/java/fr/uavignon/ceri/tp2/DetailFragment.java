package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private DetailViewModel viewModel;

    RecyclerAdapter adapt;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments()); //récupère l'id
        //Book book = Book.books[(int)args.getBookNum()];



            textTitle = (EditText) view.findViewById(R.id.nameBook);
            textAuthors = (EditText) view.findViewById(R.id.editAuthors);
            textYear = (EditText) view.findViewById(R.id.editYear);
            textGenres = (EditText) view.findViewById(R.id.editGenres);
            textPublisher = (EditText) view.findViewById(R.id.editPublisher);

            if(args.getBookNum() >= 0) {
            viewModel.SetABook(args.getBookNum());
            Book book = viewModel.getABook().getValue(); //récuperer l'instance du livre

            textTitle.setText(book.getTitle());
            textAuthors.setText(book.getAuthors());
            textYear.setText(book.getYear());
            textGenres.setText(book.getGenres());
            textPublisher.setText(book.getPublisher());
        }
        observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(!textAuthors.getText().toString().equals("") && !textGenres.getText().toString().equals("") && !textPublisher.getText().toString().equals("") && !textTitle.getText().toString().equals("") && !textAuthors.getText().toString().equals("") && !textYear.getText().toString().equals("")) {
                   viewModel.InsertOrUpdateBook(args.getBookNum(), textAuthors.getText().toString(), textGenres.getText().toString(), textPublisher.getText().toString(), textTitle.getText().toString(), textYear.getText().toString());
                   //(String auth, String genres, String publisher, String title, String year)
               }
               else
               {
                   Snackbar.make(view, "Il manque au moins un champ !", Snackbar.LENGTH_LONG)
                           .setAction("Action", null).show();
               }

            }
        });
    }

    private void observerSetup()
    {
        viewModel.getABook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(@Nullable Book books) {
                            textTitle.setText(books.getTitle());
                            textAuthors.setText(books.getAuthors());
                            textYear.setText(books.getYear());
                            textGenres.setText(books.getGenres());
                            textPublisher.setText(books.getPublisher());




                    }
                });


    }

}