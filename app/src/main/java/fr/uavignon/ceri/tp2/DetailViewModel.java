package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;

    private MutableLiveData<Book> Abook;

    public DetailViewModel (Application application)
    {
        super(application);
        repository = new BookRepository(application);
        Abook = repository.getSearchResults();



    }

    LiveData<Book> getABook()
    {
        return Abook;
    }

    public void SetABook(long id) {
        repository.findBook(id);
        Abook = repository.getSearchResults(); }

    public void InsertOrUpdateBook(long bookNum, String auth, String genres, String publisher, String title, String year) {


        if(bookNum != -1) {
            Book b = Abook.getValue();
            b.setAuthors(auth);
            b.setGenres(genres);
            b.setPublisher(publisher);
            b.setTitle(title);
            b.setYear(year);
            Abook.setValue(b);
            repository.updateBook(b);
        }
        else
        {
            Book b = new Book(title, auth, year, genres, publisher);
            // String title, String authors, String year, String genres, String publisher
            Abook.setValue(b);
            repository.insertBooks(b);
        }
    }

}







