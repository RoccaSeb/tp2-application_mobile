package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.graphics.Interpolator;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<Book> selectedBook = new MutableLiveData<Book>();

    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application Application)
    {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(Application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();

    }

    public LiveData<List<Book>> getAllBooks()
    {
        return allBooks;
    }

    public MutableLiveData<Book> getSearchResults()
    {
        return selectedBook;
    }

    public void insertBooks(Book newbook)
    {
        databaseWriteExecutor.execute(() ->  {
            bookDao.insertBook(newbook);
        });
    }

    public void deleteBook(long id)
    {
        databaseWriteExecutor.execute(() -> {
           bookDao.deleteBook(id);
        });
    }

    public void findBook(long id)
    {
        Future<Book> fbook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);

        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }


    }

    public void updateBook(Book name)
    {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(name);
        });
    }
}
