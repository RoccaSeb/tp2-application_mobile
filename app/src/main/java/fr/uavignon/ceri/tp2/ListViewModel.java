package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel { //DIAPO 84/111
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    private LiveData<Book> justABook;

    public ListViewModel (Application application)
    {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();

    }

    //MutableLiveData<List<Books>>

    LiveData<List<Book>> getAllBooks()
    {
        return allBooks;
    }

    public void delABook(long id)
    {
        repository.deleteBook(id);
    }

    LiveData<Book> getABook()
    {
        return justABook;
    }
    //public void insertBook()
}
